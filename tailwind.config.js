/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#1d1e30",
        secondary: "#e72222e0",
      },
    },
  },
  plugins: [],
};
