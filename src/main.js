import Vue from "vue";
import router from "./router";
import store from "./store";
import Axios from "axios";
import mdiVue from "mdi-vue/v2";
import * as mdijs from "@mdi/js";
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";
import App from "./App.vue";
import "./assets/css/app.css";

Vue.use(VueToast);
Vue.use(mdiVue, {
  icons: mdijs,
});

Axios.defaults.baseURL = "https://api.realworld.io/api/";

Vue.prototype.axios = Axios;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
