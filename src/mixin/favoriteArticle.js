export default {
  methods: {
    favoriteArticle(slug) {
      this.axios.post(`articles/${slug}/favorite`).then((res) => {
        this.updateArticleFavoriteValue(res.data.article);
      });
    },

    unFavoriteArticle(slug) {
      this.axios.delete(`articles/${slug}/favorite`).then((res) => {
        this.updateArticleFavoriteValue(res.data.article);
      });
    },

    updateArticleFavoriteValue(article) {
      this.$emit("updateArticleFavoriteValue", article);
    },
  },
};
