import moment from "moment";

export default {
  data: () => ({
    moment: moment,
  }),

  methods: {
    formatDate(date) {
      return moment(date).format("YYYY-MM-DD HH:MM");
    },
  },
};
