import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";

import HomeView from "@/views/HomeView.vue";
import LoginView from "@/views/auth/LoginView.vue";
import RegisterView from "@/views/auth/RegisterView.vue";
import UserSettings from "@/views/auth/UserSettings.vue";
import ArticleView from "@/views/article/ArticleView.vue";
import CreateArticle from "@/views/article/CreateArticle.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/login",
    name: "login",
    component: LoginView,
    meta: {
      guest: true,
    },
  },
  {
    path: "/register",
    name: "register",
    component: RegisterView,
    meta: {
      guest: true,
    },
  },
  {
    path: "/user-settings",
    name: "user-settings",
    component: UserSettings,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/article/:articleSlug",
    name: "article",
    component: ArticleView,
  },
  {
    path: "/create-article",
    name: "create-article",
    component: CreateArticle,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// check authentication for ( create-article and user-settings ) routes
// check guest for ( login and register ) routes
router.beforeEach((to, from, next) => {
  // read token from cookie and set in store
  store.dispatch("initAuth");
  const isAuthenticated = store.getters["isAuthenticated"];

  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!isAuthenticated) {
      next({ path: "/login" });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (isAuthenticated) {
      next({ path: "/" });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
