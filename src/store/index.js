import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";
import cookie from "vue-cookies";

Vue.use(Vuex);

const TOKEN_LOCAL_KEY = "key_for_cookie";
const getDefaultState = () => {
  return {
    data: {
      token: null,
      expire: null,
    },
  };
};

export default new Vuex.Store({
  state: getDefaultState(),

  getters: {
    isAuthenticated(state) {
      return state.data.token !== null;
    },

    getToken(state) {
      return state.data.token;
    },
  },

  mutations: {
    SET_TOKEN(state, payload) {
      Vue.set(state, "data", payload);
    },

    CLEAR_TOKEN(state) {
      Object.assign(state, getDefaultState());
    },
  },

  actions: {
    saveToken({ dispatch }, { access_token, expires_in }) {
      const expire = new Date().getTime() + Number.parseInt(expires_in) * 1000;
      const tokenData = {
        token: access_token,
        expire,
      };
      cookie.set(TOKEN_LOCAL_KEY, tokenData, {
        path: "/",
        maxAge: 60 * 60 * 24 * 180,
      });
      dispatch("initAuth");
    },

    clearToken({ commit }) {
      commit("CLEAR_TOKEN");
      cookie.remove(TOKEN_LOCAL_KEY, { path: "/" });
      Axios.defaults.headers.common["Authorization"] = "";
    },

    initAuth({ commit }) {
      const tokenData = cookie.get(TOKEN_LOCAL_KEY);
      if (tokenData?.token) {
        commit("SET_TOKEN", tokenData);
        Axios.defaults.headers.common["Authorization"] =
          "Bearer " + tokenData.token;
      } else {
        commit("CLEAR_TOKEN");
      }
    },

    logout({ dispatch }) {
      dispatch("clearToken");
      window.location.replace("/");
    },
  },
  modules: {},
});
